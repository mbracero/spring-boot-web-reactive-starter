package org.mbracero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebReactiveStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebReactiveStarterApplication.class, args);
	}
}
